import React from "react";
import "./App.css";

function App() {
  let urlCalback = window.location.href;
  let client_id = "nodejs-microservice";
  let client_secret = "6f30f9cb-11ee-4943-a8a9-059614b30a35";
  let url = `http://localhost:8000/?client_id=${client_id}&client_secret=${client_secret}&url=${urlCalback}`;
  return (
    <div>
      <div className="d-flex justify-content-center pt-5">
        <a className="nav-item nav-link" href={url}>
          Login
        </a>
      </div>
      <div className="d-flex justify-content-center pt-5">
        <textarea
          className="form-control"
          rows="15"
          defaultValue={window.location.search}
        ></textarea>
      </div>
    </div>
  );
}

export default App;
