const path = require("path");
const express = require("express");
// const session = require("express-session");
const bodyParser = require("body-parser");
const Keycloak = require("keycloak-connect");
const cors = require("cors");
const routs = require("./routs");
const app = express();
const port = 8000;

app.use(express.static(path.join(__dirname, "..", "build")));

app.use(bodyParser.json());
app.use(cors());
app.use(routs);

app.listen(port, function () {
  console.log(`Started at port ${port}`);
});
