const routs = require("express").Router();
const controller = require("../cotroller/cotroller");

routs.post("/register", controller.register);
routs.post("/signIn", controller.signIn);
routs.post("/signOut", controller.signOut);
routs.post("/checkSession", controller.checkSession);
routs.post("/exchangeToken", controller.exchangeToken);
routs.post("/forgotPassword", controller.forgotPassword);

module.exports = routs;
