const jwt_decode = require("jwt-decode");

function decode(payload) {
  return jwt_decode(payload);
}

module.exports = decode;
