const qs = require("qs");
const axios = require("axios");
const baseUrl = "http://localhost:8080";

// KEYCLOAK REST API
class Controller {
  static signIn(req, res) {
    let payload = {
      grant_type: "password",
      client_id: req.body.client_id,
      client_secret: req.body.client_secret,
      username: req.body.username,
      password: req.body.password,
    };
    axios({
      method: "post",
      url: `${baseUrl}/auth/realms/Rumah-Siap-Kerja/protocol/openid-connect/token`,
      data: qs.stringify(payload),
      headers: {
        "content-type": "application/x-www-form-urlencoded;charset=utf-8",
      },
    })
      .then(({ data }) => {
        res.status(200).json(data);
      })
      .catch((err) => {
        console.info({
          status: err.response.status,
          message: err.response.data.error_description,
        });
        res
          .status(err.response.status)
          .json({ data: { message: err.response.data.error_description } });
      });
  }

  static register(req, res) {
    let dataMaster = {
      client_secret: "240b3dea-ce16-47a5-9719-cb601a49fa98",
      grant_type: "client_credentials",
      client_id: "admin-cli",
    };

    let dataUser = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      username: req.body.usernameReg,
      email: req.body.email,
      enabled: "true",
      credentials: [
        {
          type: "password",
          value: req.body.password,
          temporary: false,
        },
      ],
      attributes: { phoneNumber: req.body.phoneNumber },
    };

    let dataLogin = {
      grant_type: "password",
      client_id: req.body.client_id,
      client_secret: req.body.client_secret,
      username: req.body.email,
      password: req.body.password,
    };

    axios({
      method: "post",
      url: `${baseUrl}/auth/realms/master/protocol/openid-connect/token`,
      data: qs.stringify(dataMaster),
      headers: {
        "content-type": "application/x-www-form-urlencoded;charset=utf-8",
      },
    })
      .then(({ data }) => {
        return axios({
          method: "post",
          url: `${baseUrl}/auth/admin/realms/Rumah-Siap-Kerja/users`,
          data: JSON.stringify(dataUser),
          headers: {
            "content-type": "application/json",
            authorization: `Bearer ${data.access_token}`,
          },
        });
      })
      .then(({ data }) => {
        return axios({
          method: "post",
          url: `${baseUrl}/auth/realms/Rumah-Siap-Kerja/protocol/openid-connect/token`,
          data: qs.stringify(dataLogin),
          headers: {
            "content-type": "application/x-www-form-urlencoded;charset=utf-8",
          },
        });
      })
      .then(({ data }) => {
        res.status(201).json(data);
      })
      .catch((err) => {
        console.info({
          status: err.response.status,
          message: err.response.data.error_description,
        });
        res
          .status(err.response.status)
          .json({ data: { message: err.response.data.error_description } });
      });
  }

  static signOut(req, res) {
    axios({
      method: "post",
      url: `${baseUrl}/auth/realms/Rumah-Siap-Kerja/protocol/openid-connect/logout`,
      data: qs.stringify({
        client_id: "nodejs-microservice",
        refresh_token: req.body.refresh_token,
        client_secret: "4d985be1-2268-4083-a180-b362cda87a10",
      }),
      headers: {
        "content-type": "application/x-www-form-urlencoded;charset=utf-8",
        authorization: `Bearer ${req.body.access_token}`,
      },
    })
      .then(() => {
        res.json({ message: "logout" });
      })
      .catch((err) => {
        console.info({
          status: err.response.status,
          message: err.response.data.error_description,
        });
        res
          .status(err.response.status)
          .json({ data: { message: err.response.data.error_description } });
      });
  }

  static checkSession(req, res) {
    axios({
      method: "get",
      url: `${baseUrl}/auth/realms/Rumah-Siap-Kerja/protocol/openid-connect/userinfo`,
      headers: {
        Authorization: `Bearer ${req.body.access_token}`,
      },
    })
      .then(({ data }) => {
        res.status(200).json(data);
      })
      .catch((err) => {
        console.info({
          status: err.response.status,
          message: err.response.data.error_description,
        });
        res
          .status(err.response.status)
          .json({ data: { message: err.response.data.error_description } });
      });
  }

  static exchangeToken(req, res) {
    let body = {
      client_id: req.body.client_id,
      client_secret: req.body.client_secret,
      grant_type: "urn:ietf:params:oauth:grant-type:token-exchange",
      subject_token_type: "urn:ietf:params:oauth:token-type:access_token",
      requested_token_type: "urn:ietf:params:oauth:token-type:refresh_token",
      subject_token: req.body.access_token,
      subject_issuer: "google",
    };

    axios({
      method: "post",
      url: `${baseUrl}/auth/realms/Rumah-Siap-Kerja/protocol/openid-connect/token`,
      data: qs.stringify(body),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
      .then(({ data }) => {
        res.status(200).json(data);
      })
      .catch((err) => {
        console.info({
          status: err.response.status,
          message: err.response.data.error_description,
        });
        res
          .status(err.response.status)
          .json({ data: { message: err.response.data.error_description } });
      });
  }

  static forgotPassword(req, res) {
    let access_token;
    let dataMaster = {
      client_secret: "240b3dea-ce16-47a5-9719-cb601a49fa98",
      grant_type: "client_credentials",
      client_id: "admin-cli",
    };

    axios({
      method: "post",
      url: `${baseUrl}/auth/realms/master/protocol/openid-connect/token`,
      data: qs.stringify(dataMaster),
      headers: {
        "content-type": "application/x-www-form-urlencoded;charset=utf-8",
      },
    })
      .then(({ data }) => {
        access_token = data.access_token;

        return axios({
          method: "get",
          url: `${baseUrl}/auth/admin/realms/Rumah-Siap-Kerja/users`,
          headers: {
            "content-type": "application/json",
            authorization: `Bearer ${access_token}`,
          },
        });
      })
      .then(({ data }) => {
        const result = data.filter((dt) => dt.username === req.body.username);
        let id;
        if (result.length > 0) {
          id = result[0].id;
        }

        return axios({
          method: "put",
          url: `${baseUrl}/auth/admin/realms/Rumah-Siap-Kerja/users/${id}/execute-actions-email`,
          data: '["UPDATE_PASSWORD"]',
          headers: {
            "content-type": "application/json",
            authorization: `Bearer ${access_token}`,
          },
        });
      })
      .then(() => {
        res.json({ message: "email sent" });
      })
      .catch((err) => {
        console.info({
          status: err.response.status,
          message: err.response.data.error_description,
        });
        res
          .status(err.response.status)
          .json({ data: { message: err.response.data.error_description } });
      });
  }
}

module.exports = Controller;
