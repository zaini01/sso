import axios from "axios";

const baseUrl = "http://localhost:8000";

export function signIn(payload, urlCallBack) {
  return async (dispatch) => {
    try {
      let response = await axios.post(`${baseUrl}/signIn`, payload);
      dispatch(setResult(response.data));
      if (response.data.access_token) {
        window.location.assign(
          `${urlCallBack}?token=${response.data.access_token}`
        );
      }
    } catch (error) {
      console.log(error);
    }
  };
}

export function signInGoogle(payload, urlCallBack) {
  return async (dispatch) => {
    try {
      let response = await axios.post(`${baseUrl}/exchangeToken`, payload);
      dispatch(setResult(response.data));
      if (response.data.access_token) {
        window.location.assign(
          `${urlCallBack}?token=${response.data.access_token}`
        );
      }
    } catch (error) {
      console.log(error);
    }
  };
}

export function register(payload, urlCallBack) {
  return async (dispatch) => {
    try {
      let response = await axios.post(`${baseUrl}/register`, payload);
      dispatch(setResult(response.data));
      if (response.data.access_token) {
        window.location.assign(
          `${urlCallBack}?token=${response.data.access_token}`
        );
      }
    } catch (error) {
      console.log(error);
    }
  };
}

export function forgotPassword(payload) {
  return async (dispatch) => {
    try {
      let response = await axios.post(`${baseUrl}/forgotPassword`, payload);
      dispatch(setResult(response.data));
    } catch (error) {
      console.log(error);
    }
  };
}

export function setUrlCallBack(payload) {
  return {
    type: "SET_URL_CALLBACK",
    payload: payload,
  };
}

export function setClient_id(payload) {
  return {
    type: "SET_CLIENT_ID",
    payload: payload,
  };
}

export function setClient_secret(payload) {
  return {
    type: "SET_CLIENT_SECRET",
    payload: payload,
  };
}

function setResult(payload) {
  return {
    type: "SET_RESULT",
    payload: payload,
  };
}
