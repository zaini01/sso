const initialState = {
  result: {},
  urlCallBack: "",
  client_id: "",
  client_secret: "",
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case "SET_RESULT":
      return { ...state, result: action.payload };
    case "SET_URL_CALLBACK":
      return { ...state, urlCallBack: action.payload };
    case "SET_CLIENT_ID":
      return { ...state, client_id: action.payload };
    case "SET_CLIENT_SECRET":
      return { ...state, client_secret: action.payload };
    default:
      return state;
  }
}
export default reducer;
