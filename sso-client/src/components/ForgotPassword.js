import React, { useState } from "react";
import { forgotPassword } from "../store/actions/action";
import { useDispatch } from "react-redux";

function ForgotPassword() {
  //EMAIL AS USER NAME SET IN KEYCLOAK
  const [username, setUserName] = useState("");
  const dispatch = useDispatch();

  const handleSubmitEmail = async (e) => {
    e.preventDefault();
    let payload = {
      username: username,
    };
    dispatch(forgotPassword(payload));
  };

  return (
    <div className="container">
      <form onSubmit={handleSubmitEmail}>
        <div className="form-group">
          <label>Alamat Email</label>
          <input
            type="email"
            className="form-control p-3"
            value={username}
            placeholder="Alamat Email"
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>

        <div className="pt-3">
          <button type="submit" className="btn btn-primary btn-block p-3">
            Submit
          </button>
        </div>
      </form>
    </div>
  );
}

export default ForgotPassword;
