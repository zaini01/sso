import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { signInGoogle } from "../store/actions/action";
import { GoogleLogin } from "react-google-login";

const clientIdGoogle =
  "975315311294-dgbt6h7gj2tdiufmmv92iopfm7d5dks7.apps.googleusercontent.com";

function GoogleSignIn() {
  const urlCallBack = useSelector((state) => state.urlCallBack);
  const client_id = useSelector((state) => state.client_id);
  const client_secret = useSelector((state) => state.client_secret);
  const dispatch = useDispatch();

  const onSuccess = (res) => {
    let payload = {
      access_token: res.accessToken,
      client_id: client_id,
      client_secret: client_secret,
    };
    dispatch(signInGoogle(payload, urlCallBack));
  };

  const onFailure = (res) => {
    console.log("Login failed: res:", res);
  };

  return (
    <div>
      <GoogleLogin
        clientId={clientIdGoogle}
        buttonText="Gunakan akun google"
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={"single_host_origin"}
        style={{ marginTop: "100px" }}
        isSignedIn={true}
      />
    </div>
  );
}

export default GoogleSignIn;
