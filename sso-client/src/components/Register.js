import React, { useState } from "react";
import { register } from "../store/actions/action";
import { useDispatch, useSelector } from "react-redux";

function Register() {
  const [usernameReg, setUserNameReg] = useState("");
  const [passwordReg, setPasswordReg] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");

  const urlCallBack = useSelector((state) => state.urlCallBack);
  const client_id = useSelector((state) => state.client_id);
  const client_secret = useSelector((state) => state.client_secret);

  const dispatch = useDispatch();

  const handleSubmitReg = async (e) => {
    e.preventDefault();
    let name = usernameReg.split(" ");

    let payload = {
      firstName: name[0],
      lastName: name[name.length - 1],
      userName: usernameReg,
      email: email,
      password: passwordReg,
      phoneNumber: phoneNumber,
      client_id: client_id,
      client_secret: client_secret,
    };

    dispatch(register(payload, urlCallBack));
    clear();
  };

  const clear = () => {
    setUserNameReg("");
    setPasswordReg("");
    setPhoneNumber("");
    setEmail("");
  };

  return (
    <div className="container-fluid">
      <form onSubmit={handleSubmitReg}>
        <div className="form-group">
          <label>Nama Lengkap</label>
          <input
            className="form-control p-3"
            value={usernameReg}
            placeholder="Nama Lengkap"
            onChange={(e) => setUserNameReg(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>Nomor Handphone</label>
          <input
            type="number"
            className="form-control p-3"
            value={phoneNumber}
            placeholder="Nomor Handphone"
            onChange={(e) => setPhoneNumber(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>Alamat Email</label>
          <input
            type="email"
            value={email}
            className="form-control p-3"
            placeholder="Alamat Email"
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>Kata Sandi</label>
          <input
            type="password"
            value={passwordReg}
            className="form-control p-3"
            placeholder="Kata Sandi"
            onChange={(e) => setPasswordReg(e.target.value)}
          />
        </div>

        <div className="d-flex ">
          <button type="submit" className="btn btn-primary btn-block">
            Daftar
          </button>
        </div>
      </form>
    </div>
  );
}

export default Register;
