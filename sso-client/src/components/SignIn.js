import React, { useState, useEffect } from "react";
import {
  signIn,
  forgotPassword,
  setUrlCallBack,
  setClient_id,
  setClient_secret,
} from "../store/actions/action";
import { useDispatch, useSelector } from "react-redux";
import queryString from "query-string";

function SignIn() {
  //EMAIL AS USER NAME SET IN KEYCLOAK
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const urlCallBack = useSelector((state) => state.urlCallBack);
  const client_id = useSelector((state) => state.client_id);
  const client_secret = useSelector((state) => state.client_secret);

  const dispatch = useDispatch();

  useEffect(() => {
    if (urlCallBack === "") {
      let query = queryString.parse(window.location.search);

      dispatch(setUrlCallBack(query.url));
      dispatch(setClient_id(query.client_id));
      dispatch(setClient_secret(query.client_secret));
    }
  }, []);

  const handleSubmitLogin = async (e) => {
    e.preventDefault();
    let payload = {
      client_id: client_id,
      client_secret: client_secret,
      username: username,
      password: password,
    };
    dispatch(signIn(payload, urlCallBack));
    setPassword("");
  };

  const handleSubmitEmail = async (e) => {
    e.preventDefault();
    let payload = {
      username: username,
    };
    dispatch(forgotPassword(payload));
  };

  return (
    <div className="container">
      <form onSubmit={handleSubmitLogin}>
        <div className="form-group">
          <label>Alamat Email</label>
          <input
            type="email"
            className="form-control p-3"
            value={username}
            placeholder="Alamat Email"
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label>Kata Sandi</label>
          <input
            type="password"
            value={password}
            className="form-control p-3"
            placeholder="Kata Sandi"
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="d-flex justify-content-end">
          <a href="#" onClick={handleSubmitEmail}>
            Lupa Kata Sandi?
          </a>
        </div>
        <div className="pt-3">
          <button type="submit" className="btn btn-primary btn-block p-3">
            Masuk
          </button>
        </div>
      </form>
    </div>
  );
}

export default SignIn;
